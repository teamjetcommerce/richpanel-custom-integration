# Integrating Richpanel with a custom website

_Developers only guide._

**:warning: :warning: :warning:
We strongly recommend doing this on Staging/Beta environment first to make sure the integration is smooth and nothing breaks down.**

Richpanel provides a javascript library for integration with your custom e-commerce solution. 


## Front-end Richpanel integration ##

_You should add the Richpanel javascript snippet on every page you want to track!_

#### Installing the javascript library ####

Let's start by including the javascript snippet (bellow) right before closing the `head` tag.

```
<script type="text/javascript">
    window.richpanel||(window.richpanel=[]),window.richpanel.queue=[],window.richpanel.methods=["track","debug","atr"],window.richpanel.skelet=function(e){return function(){a=Array.prototype.slice.call(arguments),a.unshift(e),window.richpanel.queue.push(a)}};for(var i=0;window.richpanel.methods.length>i;i++){var mthd=window.richpanel.methods[i];window.richpanel[mthd]=window.richpanel.skelet(mthd)}window.richpanel.load=function(e){var n=document,i=n.getElementsByTagName("script")[0],a=n.createElement("script");a.type="text/javascript",a.async=!0;a.src="https://api.richpanel.com/v2/j/"+e+"?version=2.0.0",i.parentNode.insertBefore(a,i)},richpanel.load("<YOUR_PROJECT_TOKEN>");
</script>
```

Consider the `richpanel.load("<YOUR_PROJECT_TOKEN>")` line. Here you should put your project token. You can find it on the Connector section in the Richpanel website.

#### Tracking Events

Events can be tracked as follows:
```javascript
    var event_type = 'view_product'
    var properties = {
        id: '312',
        price: 134.50,
        name: 'Cool sunglasses',
        url: 'http://fanstore-johnybravo.com/product/coolsunglasses',
        image_url: 'http://fanstore-johnybravo.com/product/coolsunglasses/coolsunglasses.jpg'
    }
    var user_properties = {
        uid: 'johnybravo@gmail.com',
        name: 'Johny Bravo', 
        first_name: 'Johny', 
        last_name: 'Bravo', 
        email: 'johnybravo@gmail.com'
    }
    var time = Date.now()

    richpanel.track(event_type, properties, user_properties, time)
```

Here `properties`, `user_properties` and `time` are optional.

#### Richpanel can track all types of event, few of them are defined below

- Page View - `page_view`
- Product View - `view_product`
- Cart View - `view_cart`
- Checkout Start - `checkout_start`
- Identify - `identify`
- Order - `order`
- Order Status Update - `order_status_update`
- Batch Events - `send_batch`
- Subscription - `Subscription`
- Subscription Status Update - `subscription_status_update`
- Category View - `view_category`
- Conversation Started - `conversation_started`
- Conversation Updated - `conversation_updated`

## Back-end Richpanel integration ##

You should integrate your back-end with Metrilo for two main reasons:

- Importing orders from the database
- Syncing order data. This means doing API call on creating and updating of the order.

Back-end synchronization is important part of the implementation, since it gives opportunity to track changes to the order statuses.

Make a back-end call when creating order is done on the client as well. It's ensuring that if an order sent from client fails for a reason we can have it from your back-end.