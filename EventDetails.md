# Event Details

## 2.0.0

### Event Structure

- Tracking Fields (Not Including Common Fields)

```
{
    event,
    [properties],
    [userProperties],
    [did],
    [sid],
    appClientId
}
```

- Backend Fields :
```
{
    [platform],
    [events]
}
```

- Common Fields

```
{
    [context]: {
        [device] : {
            type,
            isMobile
        },
        [ip]: {
            [systemIP]
            [networkIP]
        },
        [location] : {
            latitude,
            longitude
        }
        [os]: {
            [name],
            [version]
        }
        [page]: {
            url,
            path,
            [referrer],
            [title],
            [qs]
        },
        [screen]: {
            width,
            height,
            [density]
        },
        [browser]: {
            [name],
            [version],
            [family],
            [lang],
            [cookies]
        },
        [timezone],
        [userAgent]
    },
    [time] : {
        [sentAt],
        [originalTimestamp],
        receivedAt,
        ingestedAt,
    },
    eventId,
    [version]
}
```

- Page View

```
{
    "event": "page_view",
    "properties": {
        "name": "Products"
    },
    "userProperties": {
        "uid": "shubhanshu01@richpanel.com",
        "email": "shubhanshu01@richpanel.com",
        "name": "shubhanshu01",
        "firstName": "Shubhanshu",
        "lastName": "Sharma",
        "lastLogin": "2018-10-15 13:13:04",
        "facebook": "",
        "twitter": "",
        "linkedin": "",
        "instagram": "",
        "pinterest": "",
        "tumblr": "",
        "googleplus": "",
        "billingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "CA",
            "country": "US",
            "email": "shubhanshu01@richpanel.com",
            "postcode": "12345",
            "phone": "9977090117",
            "address1": "blah blah",
            "address2": ""
        },
        "shippingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "CA",
            "country": "US",
            "email": "",
            "postcode": "12345",
            "phone": "",
            "address1": "blah blah",
            "address2": ""
        }
    },
    "time": {
        "sentAt": 1539630131678
    },
    "context": {
        "device": {
            "type": "Desktop",
            "isMobile": 0
        },
        "ip": {
            "networkIP": "182.72.83.130"
        },
        "location": {},
        "os": {
            "name": "Mac OS X",
            "version": "10.14"
        },
        "page": {
            "url": "http://omega3innovations.swiftpim.in/store/",
            "path": "/store/",
            "referrer": "http://omega3innovations.swiftpim.in/",
            "title": "Products",
            "qs": false
        },
        "screen": {
            "width": 1440,
            "height": 900,
            "density": 24
        },
        "browser": {
            "name": "Chrome 69",
            "version": "69",
            "family": "Chrome",
            "lang": "en-US",
            "cookies": 1
        },
        "timezone": "Asia/Calcutta",
        "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
    },
    "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
    "did": "e9a1d4e2-dac2-4911-b28c-a5a75ec20cc9",
    "sid": "10825898-f698-4f27-9553-f863d055d8b1"
}
```

- Product View

```
{
    "event": "view_product",
    "properties": {
        "id": 407,
        "price": "19",
        "url": "http://omega3innovations.swiftpim.in/omega-cure-extra-strength/",
        "sku": "",
        "name": "Omega Cure Extra Strength",
        "image_url": [
            "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/11/06095459/2016-11-03-1.png"
        ],
        "categories": [
            {
                "id": 232,
                "name": "Omega Cure",
                "parent": [
                    {
                        "id": 252,
                        "name": "Products"
                    }
                ]
            }
        ]
    },
    "time": {
        "sentAt": 1539627020151
    },
    "context": {
        "device": {
            "type": "Desktop",
            "isMobile": 0
        },
        "ip": {
            "networkIP": "182.72.83.130",
            "systemIP": "10.124.92.182"
        },
        "location": {},
        "os": {
            "name": "Mac OS X",
            "version": "10.14"
        },
        "page": {
            "url": "http://omega3innovations.swiftpim.in/omega-cure-extra-strength/",
            "path": "/omega-cure-extra-strength/",
            "referrer": "http://omega3innovations.swiftpim.in/store/",
            "title": "Omega Cure Extra Strength",
            "qs": false
        },
        "screen": {
            "width": 1440,
            "height": 900,
            "density": 24
        },
        "browser": {
            "name": "Chrome 69",
            "version": "69",
            "family": "Chrome",
            "lang": "en-US",
            "cookies": 1
        },
        "timezone": "Asia/Calcutta",
        "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
    },
    "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
    "did": "e9a1d4e2-dac2-4911-b28c-a5a75ec20cc9",
    "sid": "10825898-f698-4f27-9553-f863d055d8b1"
}
```

- View Cart

```
{
    "event": "view_cart",
    "properties": [],
    "time": {
        "sentAt": 1539627159940
    },
    "context": {
        "device": {
            "type": "Desktop",
            "isMobile": 0
        },
        "ip": {
            "networkIP": "182.72.83.130",
            "systemIP": "10.124.92.182"
        },
        "location": {},
        "os": {
            "name": "Mac OS X",
            "version": "10.14"
        },
        "page": {
            "url": "http://omega3innovations.swiftpim.in/cart/",
            "path": "/cart/",
            "referrer": "http://omega3innovations.swiftpim.in/omega-cure-extra-strength/",
            "title": "Cart",
            "qs": false
        },
        "screen": {
            "width": 1440,
            "height": 900,
            "density": 24
        },
        "browser": {
            "name": "Chrome 69",
            "version": "69",
            "family": "Chrome",
            "lang": "en-US",
            "cookies": 1
        },
        "timezone": "Asia/Calcutta",
        "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
    },
    "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
    "did": "e9a1d4e2-dac2-4911-b28c-a5a75ec20cc9",
    "sid": "10825898-f698-4f27-9553-f863d055d8b1"
}
```

- Checkout Start

```
{
    "event": "checkout_start",
    "properties": [],
    "time": {
        "sentAt": 1539627167333
    },
    "context": {
        "device": {
            "type": "Desktop",
            "isMobile": 0
        },
        "ip": {
            "networkIP": "182.72.83.130",
            "systemIP": "10.124.92.182"
        },
        "location": {},
        "os": {
            "name": "Mac OS X",
            "version": "10.14"
        },
        "page": {
            "url": "http://omega3innovations.swiftpim.in/checkout/",
            "path": "/checkout/",
            "referrer": "http://omega3innovations.swiftpim.in/cart/",
            "title": "Checkout",
            "qs": false
        },
        "screen": {
            "width": 1440,
            "height": 900,
            "density": 24
        },
        "browser": {
            "name": "Chrome 69",
            "version": "69",
            "family": "Chrome",
            "lang": "en-US",
            "cookies": 1
        },
        "timezone": "Asia/Calcutta",
        "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
    },
    "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
    "did": "e9a1d4e2-dac2-4911-b28c-a5a75ec20cc9",
    "sid": "10825898-f698-4f27-9553-f863d055d8b1"
}
```

- Identify

```
{
    "event": "identify",
    "properties": [],
    "userProperties": {
        "uid": "shubhanshu01@richpanel.com",
        "email": "shubhanshu01@richpanel.com",
        "name": "shubhanshu01",
        "firstName": "Shubhanshu",
        "lastName": "Sharma",
        "lastLogin": "2018-10-15 13:13:04",
        "facebook": "",
        "twitter": "",
        "linkedin": "",
        "instagram": "",
        "pinterest": "",
        "tumblr": "",
        "googleplus": "",
        "billingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "AK",
            "country": "US",
            "email": "shubhanshu01@richpanel.com",
            "postcode": "12345",
            "phone": "9977090117",
            "address1": "blah blah",
            "address2": ""
        },
        "shippingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "AK",
            "country": "US",
            "email": "",
            "postcode": "12345",
            "phone": "",
            "address1": "blah blah",
            "address2": ""
        }
    },
    "time": {
        "sentAt": 1539627188358
    },
    "context": {
        "device": {
            "type": "Desktop",
            "isMobile": 0
        },
        "ip": {
            "networkIP": "182.72.83.130",
            "systemIP": "10.124.92.182"
        },
        "location": {},
        "os": {
            "name": "Mac OS X",
            "version": "10.14"
        },
        "page": {
            "url": "http://omega3innovations.swiftpim.in/checkout/",
            "path": "/checkout/",
            "referrer": "http://omega3innovations.swiftpim.in/checkout/",
            "title": "Checkout",
            "qs": false
        },
        "screen": {
            "width": 1440,
            "height": 900,
            "density": 24
        },
        "browser": {
            "name": "Chrome 69",
            "version": "69",
            "family": "Chrome",
            "lang": "en-US",
            "cookies": 1
        },
        "timezone": "Asia/Calcutta",
        "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
    },
    "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
    "did": "e9a1d4e2-dac2-4911-b28c-a5a75ec20cc9",
    "sid": "10825898-f698-4f27-9553-f863d055d8b1"
}
```

- Order

```
{
    "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
    "did": "e9a1d4e2-dac2-4911-b28c-a5a75ec20cc9",
    "event": "order",
    "platform": "WordPress 4.9.1 / WooCommerce 2.6.14",
    "properties": {
        "order_id": 10717,
        "order_status": "pending",
        "amount": 101.9,
        "shipping_amount": 12,
        "tax_amount": 0,
        "discount_amount": 0,
        "items": [
            {
                "id": 182,
                "price": "44.95",
                "url": "http://omega3innovations.swiftpim.in/omega-cure/",
                "sku": "1_OC",
                "name": "Omega Cure",
                "image_url": [
                    "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                    "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                    "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                    "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png"
                ],
                "categories": [
                    {
                        "id": 232,
                        "name": "Omega Cure",
                        "parent": [
                            {
                                "id": 252,
                                "name": "Products"
                            }
                        ]
                    }
                ],
                "quantity": "2"
            }
        ],
        "shipping_method": "Standard Shipping (2-3 days)",
        "payment_method": "Credit Card OLD",
        "email": "shubhanshu01@richpanel.com",
        "first_name": "Shubhanshu",
        "last_name": "Chouhan",
        "billing_phone": "9977090117",
        "billing_city": "blah",
        "billing_region": "CA",
        "billing_postcode": "12345",
        "billing_country": "US",
        "billing_address_line_1": "blah blah",
        "context": "new"
    },
    "sid": "10825898-f698-4f27-9553-f863d055d8b1",
    "time": {
        "originalTimestamp": 1539630185000,
        "sentAt": 1539630185168
    },
    "userProperties": {
        "uid": "shubhanshu01@richpanel.com",
        "email": "shubhanshu01@richpanel.com",
        "name": "shubhanshu01",
        "firstName": "Shubhanshu",
        "lastName": "Sharma",
        "lastLogin": "2018-10-15 13:13:04",
        "facebook": "",
        "twitter": "",
        "linkedin": "",
        "instagram": "",
        "pinterest": "",
        "tumblr": "",
        "googleplus": "",
        "billingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "CA",
            "country": "US",
            "email": "shubhanshu01@richpanel.com",
            "postcode": "12345",
            "phone": "9977090117",
            "address1": "blah blah",
            "address2": ""
        },
        "shippingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "CA",
            "country": "US",
            "email": "",
            "postcode": "12345",
            "phone": "",
            "address1": "blah blah",
            "address2": ""
        },
        "sourceId": 631
    },
    "version": "1.0.5"
}
```

- Order Status Update

```
{
    "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
    "did": null,
    "event": "order_status_update",
    "platform": "WordPress 4.9.1 / WooCommerce 2.6.14",
    "properties": {
        "order_id": 10717,
        "order_status": "processing",
        "amount": 101.9,
        "shipping_amount": 12,
        "tax_amount": 0,
        "discount_amount": 0,
        "items": [
            {
                "id": 182,
                "price": "44.95",
                "url": "http://omega3innovations.swiftpim.in/omega-cure/",
                "sku": "1_OC",
                "name": "Omega Cure",
                "image_url": [
                    "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                    "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                    "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                    "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png"
                ],
                "categories": [
                    {
                        "id": 232,
                        "name": "Omega Cure",
                        "parent": [
                            {
                                "id": 252,
                                "name": "Products"
                            }
                        ]
                    }
                ],
                "quantity": "2"
            }
        ],
        "shipping_method": "Standard Shipping (2-3 days)",
        "payment_method": "Credit Card OLD",
        "email": "shubhanshu01@richpanel.com",
        "first_name": "Shubhanshu",
        "last_name": "Chouhan",
        "old_status": "pending",
        "new_status": "processing",
        "billing_phone": "9977090117",
        "billing_city": "blah",
        "billing_region": "CA",
        "billing_postcode": "12345",
        "billing_country": "US",
        "billing_address_line_1": "blah blah",
        "context": "status_change"
    },
    "sid": null,
    "time": {
        "originalTimestamp": 1539630186000,
        "sentAt": 1539630187369
    },
    "userProperties": {
        "uid": "shubhanshu01@richpanel.com",
        "email": "shubhanshu01@richpanel.com",
        "name": "shubhanshu01",
        "firstName": "Shubhanshu",
        "lastName": "Sharma",
        "lastLogin": "2018-10-15 13:13:04",
        "facebook": "",
        "twitter": "",
        "linkedin": "",
        "instagram": "",
        "pinterest": "",
        "tumblr": "",
        "googleplus": "",
        "billingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "CA",
            "country": "US",
            "email": "shubhanshu01@richpanel.com",
            "postcode": "12345",
            "phone": "9977090117",
            "address1": "blah blah",
            "address2": ""
        },
        "shippingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "CA",
            "country": "US",
            "email": "",
            "postcode": "12345",
            "phone": "",
            "address1": "blah blah",
            "address2": ""
        },
        "sourceId": 631
    },
    "version": "1.0.5"
}
```

- Send Batch (Order)

```
{
    "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
    "event": "send_batch",
    "events": [
        {
            "event": "order",
            "properties": {
                "order_id": 4364,
                "order_status": "shipped",
                "amount": 30,
                "shipping_amount": 12,
                "tax_amount": 0,
                "discount_amount": 0,
                "items": [
                    {
                        "id": 414,
                        "price": "18",
                        "url": "http://omega3innovations.swiftpim.in/omega-cookie/",
                        "sku": "Om-Cookie",
                        "name": "Omega Cookie",
                        "image_url": [
                            "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/11/06095401/2016-11-07.png"
                        ],
                        "categories": [
                            {
                                "id": 235,
                                "name": "Cookies &amp; More",
                                "parent": [
                                    {
                                        "id": 252,
                                        "name": "Products"
                                    }
                                ]
                            }
                        ],
                        "quantity": "1"
                    }
                ],
                "shipping_method": "Standard Shipping (2-3 days)",
                "payment_method": "Credit Card",
                "email": "schran.julia@gmail.com",
                "first_name": "Julia",
                "last_name": "Schran",
                "billing_phone": "7242121340",
                "billing_city": "Sarasota",
                "billing_region": "FL",
                "billing_postcode": "34238",
                "billing_country": "US",
                "billing_address_line_1": "5174 Northridge Road",
                "context": "import",
                "order_type": "import"
            },
            "did": null,
            "sid": null,
            "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
            "platform": "WordPress 4.9.1 / WooCommerce 2.6.14",
            "version": "1.0.5",
            "time": {
                "originalTimestamp": 1481983523000,
                "sentAt": 1539629439595
            },
            "userProperties": {
                "uid": "schran.julia@gmail.com",
                "email": "schran.julia@gmail.com",
                "name": "schran.julia",
                "firstName": "Julia",
                "lastName": "Schran",
                "lastLogin": "",
                "facebook": "",
                "twitter": "",
                "linkedin": "",
                "instagram": "",
                "pinterest": "",
                "tumblr": "",
                "googleplus": "",
                "billingAddress": {
                    "firstName": "Julia",
                    "lastName": "Schran",
                    "city": "Sarasota",
                    "state": "FL",
                    "country": "US",
                    "email": "schran.julia@gmail.com",
                    "postcode": "34238",
                    "phone": "7242121340",
                    "address1": "5174 Northridge Road",
                    "address2": ""
                },
                "shippingAddress": {
                    "firstName": "Julia",
                    "lastName": "Schran",
                    "city": "Cranberry Township",
                    "state": "PA",
                    "country": "US",
                    "email": "",
                    "postcode": "16066",
                    "phone": "",
                    "address1": "102 Walden Pond Lane",
                    "address2": ""
                },
                "sourceId": 54
            }
        },
        {
            "event": "order",
            "properties": {
                "order_id": 4401,
                "order_status": "shipped",
                "amount": 56.95,
                "shipping_amount": 12,
                "tax_amount": 0,
                "discount_amount": 0,
                "items": [
                    {
                        "id": 182,
                        "price": "44.95",
                        "url": "http://omega3innovations.swiftpim.in/omega-cure/",
                        "sku": "1_OC",
                        "name": "Omega Cure",
                        "image_url": [
                            "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                            "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                            "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                            "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png"
                        ],
                        "categories": [
                            {
                                "id": 232,
                                "name": "Omega Cure",
                                "parent": [
                                    {
                                        "id": 252,
                                        "name": "Products"
                                    }
                                ]
                            }
                        ],
                        "quantity": "1"
                    }
                ],
                "shipping_method": "Standard Shipping (2-3 days)",
                "payment_method": "Credit Card",
                "email": "mayelise@omega3innovations.swiftpim.in",
                "first_name": "Testing",
                "last_name": "Martinsen",
                "billing_phone": "9413234229",
                "billing_city": "Brooklyn",
                "billing_region": "NY",
                "billing_postcode": "11201",
                "billing_country": "US",
                "billing_address_line_1": "189 Schermerhorn Street, Apt 24G",
                "context": "import",
                "order_type": "import"
            },
            "did": null,
            "sid": null,
            "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
            "platform": "WordPress 4.9.1 / WooCommerce 2.6.14",
            "version": "1.0.5",
            "time": {
                "originalTimestamp": 1482190753000,
                "sentAt": 1539629439606
            },
            "userProperties": {
                "uid": "mayelise@omega3innovations.swiftpim.in",
                "email": "mayelise@omega3innovations.swiftpim.in",
                "name": "mayelise",
                "firstName": "Testing",
                "lastName": "Martinsen",
                "lastLogin": "2017-01-22 18:47:19",
                "facebook": "",
                "twitter": "",
                "linkedin": "",
                "instagram": "",
                "pinterest": "",
                "tumblr": "",
                "googleplus": "",
                "billingAddress": {
                    "firstName": "Testing",
                    "lastName": "Martinsen",
                    "city": "Brooklyn",
                    "state": "NY",
                    "country": "US",
                    "email": "mayelise@omega3innovations.swiftpim.in",
                    "postcode": "11201",
                    "phone": "9413234229",
                    "address1": "189 Schermerhorn Street, Apt 24G",
                    "address2": ""
                },
                "shippingAddress": {
                    "firstName": "Testing",
                    "lastName": "Martinsen",
                    "city": "Brooklyn",
                    "state": "NY",
                    "country": "US",
                    "email": "",
                    "postcode": "11201",
                    "phone": "",
                    "address1": "189 Schermerhorn Street, Apt 24G",
                    "address2": ""
                },
                "sourceId": 36
            }
        }
    ],
    "platform": "WordPress 4.9.1 / WooCommerce 2.6.14",
    "version": "1.0.5"
}
```

- Subscription

```
{
    "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
    "did": "e9a1d4e2-dac2-4911-b28c-a5a75ec20cc9",
    "event": "subscription",
    "platform": "WordPress 4.9.1 / WooCommerce 2.6.14",
    "properties": {
        "order_id": 10720,
        "order_status": "pending",
        "amount": 288,
        "shipping_amount": 0,
        "tax_amount": 0,
        "discount_amount": 0,
        "items": [
            {
                "id": 296,
                "price": "144",
                "url": "http://omega3innovations.swiftpim.in/omega-cure-extra-strength-subscription/",
                "sku": "",
                "name": "Omega Cure Extra Strength Subscription",
                "image_url": [],
                "categories": [
                    {
                        "id": 233,
                        "name": "Omega Cure Subscription",
                        "parent": [
                            {
                                "id": 251,
                                "name": "Subscription"
                            }
                        ]
                    }
                ],
                "quantity": "2"
            }
        ],
        "shipping_method": "Free Standard Shipping (2-3 days)",
        "payment_method": "Credit Card OLD",
        "email": "shubhanshu01@richpanel.com",
        "first_name": "Shubhanshu",
        "last_name": "Chouhan",
        "billing_phone": "9977090117",
        "billing_city": "blah",
        "billing_region": "CA",
        "billing_postcode": "12345",
        "billing_country": "US",
        "billing_address_line_1": "blah blah",
        "context": "new",
        "start": "2018-10-15 19:06:06",
        "trial_end": 0,
        "next_payment": "2018-11-12 19:06:06",
        "last_payment": "2018-10-15 19:06:06",
        "end": 0
    },
    "sid": "10825898-f698-4f27-9553-f863d055d8b1",
    "time": {
        "originalTimestamp": 1477545826000,
        "sentAt": 1539630366853
    },
    "userProperties": {
        "uid": "shubhanshu01@richpanel.com",
        "email": "shubhanshu01@richpanel.com",
        "name": "shubhanshu01",
        "firstName": "Shubhanshu",
        "lastName": "Sharma",
        "lastLogin": "2018-10-15 13:13:04",
        "facebook": "",
        "twitter": "",
        "linkedin": "",
        "instagram": "",
        "pinterest": "",
        "tumblr": "",
        "googleplus": "",
        "billingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "CA",
            "country": "US",
            "email": "shubhanshu01@richpanel.com",
            "postcode": "12345",
            "phone": "9977090117",
            "address1": "blah blah",
            "address2": ""
        },
        "shippingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "CA",
            "country": "US",
            "email": "",
            "postcode": "12345",
            "phone": "",
            "address1": "blah blah",
            "address2": ""
        },
        "sourceId": 631
    },
    "version": "1.0.5"
}
```

- Subscription Status Update

```
{
    "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
    "did": null,
    "event": "subscription_status_update",
    "platform": "WordPress 4.9.1 / WooCommerce 2.6.14",
    "properties": {
        "order_id": 10720,
        "order_status": "active",
        "amount": 288,
        "shipping_amount": 0,
        "tax_amount": 0,
        "discount_amount": 0,
        "items": [
            {
                "id": 296,
                "price": "144",
                "url": "http://omega3innovations.swiftpim.in/omega-cure-extra-strength-subscription/",
                "sku": "",
                "name": "Omega Cure Extra Strength Subscription",
                "image_url": [],
                "categories": [
                    {
                        "id": 233,
                        "name": "Omega Cure Subscription",
                        "parent": [
                            {
                                "id": 251,
                                "name": "Subscription"
                            }
                        ]
                    }
                ],
                "quantity": "2"
            }
        ],
        "shipping_method": "Free Standard Shipping (2-3 days)",
        "payment_method": "Credit Card OLD",
        "email": "shubhanshu01@richpanel.com",
        "first_name": "Shubhanshu",
        "last_name": "Chouhan",
        "old_status": "pending",
        "new_status": "active",
        "billing_phone": "9977090117",
        "billing_city": "blah",
        "billing_region": "CA",
        "billing_postcode": "12345",
        "billing_country": "US",
        "billing_address_line_1": "blah blah",
        "context": "status_change"
    },
    "sid": null,
    "time": {
        "originalTimestamp": 1477545826000,
        "sentAt": 1539630369366
    },
    "userProperties": {
        "uid": "shubhanshu01@richpanel.com",
        "email": "shubhanshu01@richpanel.com",
        "name": "shubhanshu01",
        "firstName": "Shubhanshu",
        "lastName": "Sharma",
        "lastLogin": "2018-10-15 13:13:04",
        "facebook": "",
        "twitter": "",
        "linkedin": "",
        "instagram": "",
        "pinterest": "",
        "tumblr": "",
        "googleplus": "",
        "billingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "CA",
            "country": "US",
            "email": "shubhanshu01@richpanel.com",
            "postcode": "12345",
            "phone": "9977090117",
            "address1": "blah blah",
            "address2": ""
        },
        "shippingAddress": {
            "firstName": "Shubhanshu",
            "lastName": "Chouhan",
            "city": "blah",
            "state": "CA",
            "country": "US",
            "email": "",
            "postcode": "12345",
            "phone": "",
            "address1": "blah blah",
            "address2": ""
        },
        "sourceId": 631
    },
    "version": "1.0.5"
}
```

- Send Batch (subscription)

```
    {
    "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
    "event": "send_batch",
    "events": [
        {
            "event": "subscription",
            "properties": {
                "order_id": 5872,
                "order_status": "on-hold",
                "amount": 144,
                "shipping_amount": 0,
                "tax_amount": 0,
                "discount_amount": 0,
                "items": [
                    {
                        "id": 296,
                        "price": "144",
                        "url": "http://omega3innovations.swiftpim.in/omega-cure-extra-strength-subscription/",
                        "sku": "",
                        "name": "Omega Cure Extra Strength Subscription",
                        "image_url": [],
                        "categories": [
                            {
                                "id": 233,
                                "name": "Omega Cure Subscription",
                                "parent": [
                                    {
                                        "id": 251,
                                        "name": "Subscription"
                                    }
                                ]
                            }
                        ],
                        "quantity": "1"
                    }
                ],
                "shipping_method": "Free Standard Shipping (2-3 days)",
                "payment_method": "Credit Card",
                "email": "dorianb0109@gmail.com",
                "first_name": "Dorian",
                "last_name": "Balogh",
                "billing_phone": "734-589-5015",
                "billing_city": "Livonia",
                "billing_region": "MI",
                "billing_postcode": "48154-4567",
                "billing_country": "US",
                "billing_address_line_1": "29037 Lyndon",
                "context": "import",
                "order_type": "import",
                "start": "2017-01-13 01:38:13",
                "trial_end": 0,
                "next_payment": "2017-03-18 01:38:06",
                "last_payment": "2017-03-27 18:51:18",
                "end": 0
            },
            "did": null,
            "sid": null,
            "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
            "platform": "WordPress 4.9.1 / WooCommerce 2.6.14",
            "version": "1.0.5",
            "time": {
                "originalTimestamp": 1484271493000,
                "sentAt": 1539630650041
            },
            "userProperties": {
                "uid": "dorianb0109@gmail.com",
                "email": "dorianb0109@gmail.com",
                "name": "dorianb0109",
                "firstName": "Dorian",
                "lastName": "Balogh",
                "lastLogin": "",
                "facebook": "",
                "twitter": "",
                "linkedin": "",
                "instagram": "",
                "pinterest": "",
                "tumblr": "",
                "googleplus": "",
                "billingAddress": {
                    "firstName": "Dorian",
                    "lastName": "Balogh",
                    "city": "Livonia",
                    "state": "MI",
                    "country": "US",
                    "email": "dorianb0109@gmail.com",
                    "postcode": "48154-4567",
                    "phone": "734-589-5015",
                    "address1": "29037 Lyndon",
                    "address2": ""
                },
                "shippingAddress": {
                    "firstName": "Dorian",
                    "lastName": "Balogh",
                    "city": "Livonia",
                    "state": "MI",
                    "country": "US",
                    "email": "",
                    "postcode": "48154-4567",
                    "phone": "",
                    "address1": "29037 Lyndon",
                    "address2": ""
                },
                "sourceId": 269
            }
        },
        {
            "event": "subscription",
            "properties": {
                "order_id": 6052,
                "order_status": "on-hold",
                "amount": 144,
                "shipping_amount": 0,
                "tax_amount": 0,
                "discount_amount": 0,
                "items": [
                    {
                        "id": 296,
                        "price": "144",
                        "url": "http://omega3innovations.swiftpim.in/omega-cure-extra-strength-subscription/",
                        "sku": "",
                        "name": "Omega Cure Extra Strength Subscription",
                        "image_url": [],
                        "categories": [
                            {
                                "id": 233,
                                "name": "Omega Cure Subscription",
                                "parent": [
                                    {
                                        "id": 251,
                                        "name": "Subscription"
                                    }
                                ]
                            }
                        ],
                        "quantity": "1"
                    }
                ],
                "shipping_method": "",
                "payment_method": "Credit Card",
                "email": "mcoop@tir.com",
                "first_name": "Sandra",
                "last_name": "Cooper",
                "billing_phone": "(810)636-7532",
                "billing_city": "Goodrich",
                "billing_region": "MI",
                "billing_postcode": "48438",
                "billing_country": "US",
                "billing_address_line_1": "9436 Hegel Rd",
                "context": "import",
                "order_type": "import",
                "start": "2016-10-24 17:46:07",
                "trial_end": 0,
                "next_payment": "2017-02-24 13:25:29",
                "last_payment": "2017-01-13 18:52:20",
                "end": 0
            },
            "did": null,
            "sid": null,
            "appClientId": "2jeumcumih4b1ruul0njqn5d4a",
            "platform": "WordPress 4.9.1 / WooCommerce 2.6.14",
            "version": "1.0.5",
            "time": {
                "originalTimestamp": 1477331167000,
                "sentAt": 1539630650048
            },
            "userProperties": {
                "uid": "mcoop@tir.com",
                "email": "mcoop@tir.com",
                "name": "sandra_cooper",
                "firstName": "Sandra",
                "lastName": "Cooper",
                "lastLogin": "2017-01-14 18:50:03",
                "facebook": "",
                "twitter": "",
                "linkedin": "",
                "instagram": "",
                "pinterest": "",
                "tumblr": "",
                "googleplus": "",
                "billingAddress": {
                    "firstName": "Sandra",
                    "lastName": "Cooper",
                    "city": "Goodrich",
                    "state": "MI        ",
                    "country": "USA",
                    "email": "mcoop@tir.com",
                    "postcode": "48438",
                    "phone": "(810)636-7532",
                    "address1": "9436 Hegel Rd",
                    "address2": ""
                },
                "shippingAddress": {
                    "firstName": "Sandra",
                    "lastName": "Cooper",
                    "city": "Goodrich",
                    "state": "MI",
                    "country": "USA",
                    "email": "",
                    "postcode": "48438",
                    "phone": "",
                    "address1": "9436 Hegel Rd",
                    "address2": ""
                },
                "sourceId": 277
            }
        }
    ],
    "platform": "WordPress 4.9.1 / WooCommerce 2.6.14",
    "version": "1.0.5"
}

```


## 1.0.0

### Ecommerce Events


- Category View

```
	{
		event_type <Event Type>: "view_category",
    	properties <Properties>: {
			id <Category Id>,
			name <Category Name>,
			url <Category Page URL>,
			[parent],
			[is_logged_in] <is_logged_in>: true,
			[user_properties] <User Properties>
		},
		appClientId <App Client Id> (Ref: Richpanel),
	    did <Domain Id>,
		[sid] <Session Id>,
	    time <Time>,
		[userIpAddress] <IP Address>,
		[platform] <Platform>,
		[page_title] <Page Title>,
		[useragent] <Useragent>,
		[br_name] <Browser Name>,
		[br_version] <Browser Version>,
		[br_family] <Browser Family>,
		[br_lang] <Locale>,
		[br_cookies] <Browser Cookie>,
		[br_colordepth] <Color Depth>,
		[br_viewwidth] <Screen Width>,
		[br_viewheight] <Screen Height>,
		[os_name] <OS Name>,
		[os_family] <OS Family>,
		[os_version] <OS Version>,
		[os_timezone] <Timezone>,
		[dvce_type] <Device>,
		[dvce_ismobile] <Device Is Mobile>,
		[dvce_screenwidth] <Screen Width>,
		[dvce_screenheight] <Screen Height>,
		[doc_charset] <Charachter Set>,
		[doc_width] <Document With>,
		[doc_height] <Document Height>,
		[geo_timezone] <Timezone>,
		[meta] <Meta>,
		[irs] <IRS>,
		[s_meta] <S Meta>
	}
```

 - Example

	```
		{
			"event_type": "view_category",
			"properties": {
				"id": "18",
				"name": "Eyewear",
				"url": "http://magentotest.richpanel.com/accessories/eyewear.html#/",
				"uri": "http://magentotest.richpanel.com/accessories/eyewear.html#/"
			},
			"appClientId": "2s56kgaehlm4u8clcj8stu2145",
			"meta": {
				"referrer": "http://magentotest.richpanel.com/",
				"href": "http://magentotest.richpanel.com/accessories/eyewear.html#/",
				"qs": false
			},
			"did": "d86a4760-8915-4067-84d5-c0cc9abff33c",
			"sid": "11ff4a59-6066-4127-8d5c-096eef0dbf1f",
			"irs": {
				"ir": "http://magentotest.richpanel.com/",
				"ird": "richpanel.com/"
			},
			"userIpAddress": "114.143.1.178",
			"time": 1535382415388,
			"s_meta": {
				"referrer": "",
				"href": "http://magentotest.richpanel.com/",
				"qs": false
			},
			"platform": "web",
			"page_title": "Eyewear - Accessories",
			"useragent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
			"br_name": "Chrome 68",
			"br_version": "68",
			"br_family": "Chrome",
			"br_lang": "en-US",
			"br_cookies": 1,
			"br_colordepth": 24,
			"br_viewwidth": 1440,
			"br_viewheight": 540,
			"os_name": "Mac OS X 10.13.6",
			"os_family": "Mac OS X",
			"os_version": "10.13.6",
			"os_timezone": "Asia/Calcutta",
			"dvce_type": "Desktop",
			"dvce_ismobile": 0,
			"dvce_screenwidth": 1440,
			"dvce_screenheight": 900,
			"doc_charset": "UTF-8",
			"doc_width": 1440,
			"doc_height": 540,
			"geo_timezone": "Asia/Calcutta",
			"system_ip": "10.124.92.225",
			"tenantId": "speedysigns164",
			"isImported": false
		}
	```

- Product View

```
	{
		event_type <Event Type>: "view_product",
    	properties <Properties>: {
			id <Product Id>,
			name <Product Name>,
			price,
			url <Product Page URL>,
			[image_url] <Product Image URL>: [],
			[categories] <Product Categories>,
			[option_id],
			[option_image_url],
			[option_name],
			[option_price],
			[option_sku],
			[is_logged_in] <is_logged_in>,
			[user_properties] <User Properties>
		},
		appClientId <App Client Id> (Ref: Richpanel),
	    did <Domain Id>,
		[sid] <Session Id>,
	    time <Time>,
		[userIpAddress] <IP Address>,
		[platform] <Platform>,
		[page_title] <Page Title>,
		[useragent] <Useragent>,
		[br_name] <Browser Name>,
		[br_version] <Browser Version>,
		[br_family] <Browser Family>,
		[br_lang] <Locale>,
		[br_cookies] <Browser Cookie>,
		[br_colordepth] <Color Depth>,
		[br_viewwidth] <Screen Width>,
		[br_viewheight] <Screen Height>,
		[os_name] <OS Name>,
		[os_family] <OS Family>,
		[os_version] <OS Version>,
		[os_timezone] <Timezone>,
		[dvce_type] <Device>,
		[dvce_ismobile] <Device Is Mobile>,
		[dvce_screenwidth] <Screen Width>,
		[dvce_screenheight] <Screen Height>,
		[doc_charset] <Charachter Set>,
		[doc_width] <Document With>,
		[doc_height] <Document Height>,
		[geo_timezone] <Timezone>,
		[meta] <Meta>,
		[irs] <IRS>,
		[s_meta] <S Meta>
	}
```

 - Example

	```
		{
			"event_type": "view_product",
			"properties": {
				"id": 407,
				"name": "Omega Cure Extra Strength",
				"price": "19",
				"url": "http://omega3innovations.swiftpim.in/omega-cure-extra-strength/",
				"image_url": ["http://omega3innovations.swiftpim.in/wp-content/uploads/sites/10/2016/11/2016-11-03-1.png"],
				"categories": [{
					"id": 232,
					"name": "Omega Cure"
				}],
				"is_logged_in": true,
				"user_properties": null,
				"uri": "http://omega3innovations.swiftpim.in/omega-cure-extra-strength/"
			},
			"appClientId": "7kganv8tq8u07g6dbasib58e9f",
			"meta": {
				"referrer": "http://omega3innovations.swiftpim.in/",
				"href": "http://omega3innovations.swiftpim.in/omega-cure-extra-strength/",
				"qs": false
			},
			"did": "9b6adeaa-bf1b-40bb-baf6-d99b6903e60e",
			"sid": "a509e9c4-f792-4bc9-8d27-df6e2a26d41e",
			"irs": {
				"ir": "http://omega3innovations.swiftpim.in/",
				"ird": "swiftpim.in/"
			},
			"userIpAddress": "182.72.83.130",
			"time": 1535384906706,
			"s_meta": {
				"referrer": "",
				"href": "http://omega3innovations.swiftpim.in/store/",
				"qs": false
			},
			"platform": "web",
			"page_title": "Omega Cure Extra Strength",
			"useragent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
			"br_name": "Chrome 68",
			"br_version": "68",
			"br_family": "Chrome",
			"br_lang": "en-US",
			"br_cookies": 1,
			"br_colordepth": 24,
			"br_viewwidth": 1440,
			"br_viewheight": 540,
			"os_name": "Mac OS X 10.13.6",
			"os_family": "Mac OS X",
			"os_version": "10.13.6",
			"os_timezone": "Asia/Calcutta",
			"dvce_type": "Desktop",
			"dvce_ismobile": 0,
			"dvce_screenwidth": 1440,
			"dvce_screenheight": 900,
			"doc_charset": "UTF-8",
			"doc_width": 1440,
			"doc_height": 540,
			"geo_timezone": "Asia/Calcutta",
			"system_ip": "10.124.92.225"
		}
	```

- Page View

```
	{
		event_type <Event Type>: "pageview",
    	properties <Properties>: {
			name <Page Name>,
			url <Page URL>,
			[is_logged_in] <is_logged_in>: true,
			[user_properties] <User Properties>
		},
		appClientId <App Client Id> (Ref: Richpanel),
	    did <Domain Id>,
		[sid] <Session Id>,
	    time <Time>,
		[userIpAddress] <IP Address>,
		[platform] <Platform>,
		[page_title] <Page Title>,
		[useragent] <Useragent>,
		[br_name] <Browser Name>,
		[br_version] <Browser Version>,
		[br_family] <Browser Family>,
		[br_lang] <Locale>,
		[br_cookies] <Browser Cookie>,
		[br_colordepth] <Color Depth>,
		[br_viewwidth] <Screen Width>,
		[br_viewheight] <Screen Height>,
		[os_name] <OS Name>,
		[os_family] <OS Family>,
		[os_version] <OS Version>,
		[os_timezone] <Timezone>,
		[dvce_type] <Device>,
		[dvce_ismobile] <Device Is Mobile>,
		[dvce_screenwidth] <Screen Width>,
		[dvce_screenheight] <Screen Height>,
		[doc_charset] <Charachter Set>,
		[doc_width] <Document With>,
		[doc_height] <Document Height>,
		[geo_timezone] <Timezone>,
		[meta] <Meta>,
		[irs] <IRS>,
		[s_meta] <S Meta>
	}
```

 - Example

	```
		{
			"event_type": "pageview",
			"properties": {
				"name": "Products",
				"is_logged_in": true,
				"user_properties": {
					"uid": "amit@swiftpim.com",
					"email": "amit@swiftpim.com",
					"name": "jet_admin",
					"first_name": "",
					"last_name": "",
					"last_login": "2018-08-22 06:22:25",
					"facebook": "",
					"twitter": "",
					"linkedin": "",
					"instagram": "",
					"pinterest": "",
					"tumblr": "",
					"googleplus": "",
					"billing_address": {
						"first_name": "Sayak",
						"last_name": "Ganguly",
						"city": "Kolkata",
						"state": "FL",
						"country": "US",
						"email": "sayak.ganguly@esolzmail.com",
						"postcode": "02148",
						"phone": "9903115457",
						"address_1": "Ecospace Business Park",
						"address_2": ""
					},
					"shipping_address": {
						"first_name": "Sayak",
						"last_name": "Ganguly",
						"city": "Kolkata",
						"state": "FL",
						"country": "US",
						"email": "",
						"postcode": "02148",
						"phone": "",
						"address_1": "Ecospace Business Park",
						"address_2": ""
					},
					"sourceId": 1
				},
				"uri": "http://omega3innovations.swiftpim.in/store/"
			},
			"appClientId": "7kganv8tq8u07g6dbasib58e9f",
			"meta": {
				"referrer": "",
				"href": "http://omega3innovations.swiftpim.in/store/",
				"qs": false
			},
			"did": "dbe01c45-8550-4147-99fd-97b8d9e1c5d4",
			"sid": "11e1a711-7137-429e-a256-e9ccfd1ba7ec",
			"irs": {
				"ir": "direct",
				"ird": "direct"
			},
			"userIpAddress": "182.72.83.130",
			"time": 1535022151431,
			"s_meta": {
				"referrer": "",
				"href": "http://omega3innovations.swiftpim.in/store/",
				"qs": false
			},
			"platform": "web",
			"page_title": "Products",
			"useragent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
			"br_name": "Chrome 68",
			"br_version": "68",
			"br_family": "Chrome",
			"br_lang": "en-US",
			"br_cookies": 1,
			"br_colordepth": 24,
			"br_viewwidth": 1438,
			"br_viewheight": 508,
			"os_name": "Mac OS X 10.13.6",
			"os_family": "Mac OS X",
			"os_version": "10.13.6",
			"os_timezone": "Asia/Calcutta",
			"dvce_type": "Desktop",
			"dvce_ismobile": 0,
			"dvce_screenwidth": 1440,
			"dvce_screenheight": 900,
			"doc_charset": "UTF-8",
			"doc_width": 1438,
			"doc_height": 508,
			"geo_timezone": "Asia/Calcutta"
		}
	```

### Conversation Events

- Conversation Started

```
    {
        event_type <Event Type>: "conversation_started",
    	properties <Properties>: {
	    	conversationId <Conversation Id>,
			[channel] <Channel>,
			[channelId] <Channel>,
			[subject] <Subject>,
			[operatorId] <Operator Id>,
			[operatorName] <Operator>,
            [user_properties] <User Properties> : {
            	uid <User Id>,
            	email <Email>
            }
	    },
    	appClientId <App Client Id> (Ref: Richpanel),
	    did <Domain Id>,
	    time <Time>,
    }
```

- Conversation Updated

```
    {
        event_type <Event Type>: "conversation_updated",
    	properties <Properties>: {
            conversationId <Conversation Id>,
			updateType <Conversation Update Type>,
			[channel] <Channel>,
			[channelId] <Channel>,
			[subject] <Subject>,
			[operatorId] <Operator Id>,
			[operatorName] <Operator>,
            [user_properties] <User Properties> : {
            	uid <User Id>,
            	email <Email>
            }
	    },
    	appClientId <App Client Id> (Ref: Richpanel),
        did <Domain Id>,
        time <Time>
    }
```

### Richpanel Events

- Updated Persons

```
	{
		event_type <Event Type>: "updated_persons",
		properties <Properties>: {
			nodes <Person Node Ids>: []
		},
		appClientId <App Client Id> (Ref: Richpanel),
		time <Time>
	}
```

----

## Attribute Glossary

1. **Platform**
	- *Property Key* - `channel` | `platform`
	- *Description* - Its a plaform.
	- *Format* - `String`; Alphanumeric, starts with a letter only.
	- *Example* - `web`, `mobile`
2. **Channel Id**
	- *Property Key* - `channelId`
	- *Description* - AppClientID of connector.
	- *Format* - `String`; Alphanumeric, starts with a letter only.
3. **Conversation Id**
	- *Property Key* - `conversationId`
	- *Description* - Unique ID for a conversation.
	- *Format* - `UUID4`
4. **App Client Id**
	- *Property Key* - `appClientId`
	- *Description* - AppClientID of a connector.
	- *Format* - `String`; Alphanumeric
5. **Event Type**
	- *Property Key* - `event_type`
	- *Description* - Name of the event.
	- *Format* - `String`; Alphanumeric, starts with a letter only.
	- *Example* - `conversation_started`, `pageview`, `order`
6. **Domain ID**
	- *Property Key* - `did`
	- *Description* - Unique cookie generated at frontend. We keep this for atleast 3 years until `uid` is updated.
	- *Format* - `UUID4`
7. **Session ID**
	- *Property Key* - `sid`
	- *Description* - Unique session generated at frontend. We keep changing this for every ideal 90 mins or if `uid` is updated.
	- *Format* - `UUID4`
8. **Time**
	- *Property Key* - `time`
	- *Description* - Event execution time in UTC.
	- *Format* - `longint`
9. **Properties**
	- *Property Key* - `properties`
	- *Description* - JSON Object containing properties specific to event.
	- *Format* - `Object`
	- *Possible Keys* 
		- [`conversationId`] - String
		- [`channelId`] - String
		- [`channel`] - String
		- [`user_properties`] - Object
		- [`is_logged_in`] - Boolean
10. **User Properties**
	- *Property Key* - `user_properties`
	- *Description* - JSON Object containing user specific properties provided captured by system. Only available when user is logged in i.e. along with `is_logged_in` in `properties`
	- *Format* - `Object`
	- *Possible Keys* 
		- `uid` - String
    	- `sourceId` - String
		- [`email`] - String
		- [`name`] - String
		- [`first_name`] - String
		- [`last_name`] - String
		- [`shipping_address`] - String
		- [`billing_address`] - String
		- [`last_login`] - String
		- [`facebook`] - String
		- [`facebook`] - String
		- [`twitter`] - String
		- [`linkedin`] - String
		- [`instagram`] - String
		- [`pinterest`] - String
		- [`tumblr`] - String
		- [`googleplus`] - String
		- [`billing_address`] - String
		- [`shipping_address`] - String
11. **Billing Address**
	- *Property Key* - `billing_address`
	- *Description* - JSON Object containing user address
	- *Format* - `Object`
	- *Possible Keys* 
		- [`first_name`] - String
		- [`last_name`] - String
		- [`city`] - String
		- [`state`] - String
		- [`country`] - String
		- [`email`] - String
		- [`postcode`] - String
		- [`phone`] - String
		- [`address_1`] - String
		- [`address_2`] - String
12. **Shipping Address**
	- *Property Key* - `shipping_address`
	- *Description* - JSON Object containing user address
	- *Format* - `Object`
	- *Possible Keys* 
		- [`first_name`] - String
		- [`last_name`] - String
		- [`city`] - String
		- [`state`] - String
		- [`country`] - String
		- [`email`] - String
		- [`postcode`] - String
		- [`phone`] - String
		- [`address_1`] - String
		- [`address_2`] - String
13. **User Id**
	- *Property Key* - `uid`
	- *Description* - Unique user id for specific `appClientId`. Currently we're sending email as uid.
	- *Format* - `String`; Alphanumeric
14. **Source Id**
	- *Property Key* - `sourceId`
	- *Description* - Unique user id for specific `appClientId`.
	- *Format* - `String`; Alphanumeric
15. **Email**
	- *Property Key* - `email`
	- *Description* - Email of a user
	- *Format* - `String`
16. **Person Node Ids**
	- *Property Key* - `nodes`
	- *Description* - Ids of Person Nodes
	- *Format* - `Array<Integer>`
17. **Product Image URL**
	- *Property Key* - `image_url`
	- *Description* - URL of product images
	- *Format* - `Array<String>`
18. **Product Categories**
	- *Property Key* - `categories`
	- *Description* - URL of product images
	- *Format* - `Object`
	- *Possible Keys* 
		- `id` String
		- `name` String
		- `parent` Array(String)
19. **Conversation Update Type**
	- *Property Key* - `updateType`
	- *Description* - URL of product images
	- *Format* - `String`
	- *Possible Values* 
		- customer_reply
			- This event type will be sent everytime the customer replies
		- agent_assigned
			- This event will be sent whenever the conversation is assigned an agent
		- tag_added
			- This event will be sent whenever the conversation is tagged
		- priority
			- This event will be sent whenever the priority of the conversation is changed
		- operator_reply
			- This event will be sent everytime the operator replies to the customer
		- status_update
			- This event will be sent whenever the status of the conversation is changed